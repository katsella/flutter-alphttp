import 'package:alp_client/core/MetodType.dart';
import 'package:alp_client/process/AlpHttp.dart';
import 'package:alp_client/process/request/model/AlpResponse.dart';
import 'package:flutter_test/flutter_test.dart';

void main() {
  test('Get test with path variable', () async {
    AlpHttp alpHttp = AlpHttp(baseUrl: "https://jsonplaceholder.typicode.com/");

    AlpResponse<String> response = await alpHttp
        .request("/todos/{id}")
        .setMethod(MetodType.GET)
        .setPathVariable("id", 2)
        .execute<String>();

    print(response.data);
    expect(true, true);
  });

  test('Cache test', () async {
    AlpHttp alpHttp = AlpHttp(baseUrl: "https://jsonplaceholder.typicode.com/");

    AlpResponse<String> response = await alpHttp
        .request("/todos/{id}")
        .setMethod(MetodType.GET)
        .setPathVariable("id", 2)
        .addParam("name", "value")
        .setCacheEnable(true)
        .execute();

    response = await alpHttp
        .request("/todos/{id}")
        .setMethod(MetodType.GET)
        .setPathVariable("id", 2)
        .addParam("name", "value")
        .setCacheEnable(true)
        .execute();

    print(response.data);
    expect(true, true);
  });

  test('Post test', () async {
    AlpHttp alpHttp = AlpHttp(baseUrl: "https://httpbin.org");

    AlpResponse<String> response = await alpHttp
        .request("/post")
        .setMethod(MetodType.POST)
        .setBody("test")
        .execute();

    print(response.data);
    expect(true, true);
  });

  test('Put test', () async {
    AlpHttp alpHttp = AlpHttp(baseUrl: "https://httpbin.org");

    AlpResponse<String> response = await alpHttp
        .request("/put")
        .setMethod(MetodType.PUT)
        .setBody("test")
        .execute();

    print(response.data);
    expect(true, true);
  });

  test('Patch test', () async {
    AlpHttp alpHttp = AlpHttp(baseUrl: "https://httpbin.org");

    AlpResponse<String> response = await alpHttp
        .request("/patch")
        .setMethod(MetodType.PATCH)
        .setBody("test")
        .execute();

    print(response.data);
    expect(true, true);
  });

  test('Delete test', () async {
    AlpHttp alpHttp = AlpHttp(baseUrl: "https://httpbin.org");

    AlpResponse<String> response = await alpHttp
        .request("/delete")
        .setMethod(MetodType.DELETE)
        .setBody("test")
        .execute();

    print(response.data);
    expect(true, true);
  });
}
