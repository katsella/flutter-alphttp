
## Features

Let you implement rest api clean any easy way. <br>
You can enable memory caching for get requests.

## Installing

Add this dependency
```
  alp_client:
    git:
      url: https://gitlab.com/katsella/flutter-alphttp
```

## Usage

Simple to use
```dart
    AlpHttp alpHttp = AlpHttp(baseUrl: "https://test.com");
    Map<String, dynamic> headers = Map();
    headers["authentication"] = "";

    Future<String> response = alpHttp
        .request("/api/v1/query/{id}/info")
        .setMethod(MetodType.GET)
        .setPathVariable("id", 1)
        .addParam("key", "value")
        .addHeaders(headers)
        .setCacheEnable(true)
        .execute();

    response.then((value) {
      print(value);
    });
```

