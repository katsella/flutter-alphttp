class UrlUtils {
  static String combineUrl(String? base, String? path) {
    if (base == null) return path == null ? "" : path;
    if (base.endsWith('/')) base = base.substring(0, base.length - 1);
    if (path != null && !path.startsWith('/')) path = "/" + path;
    return base + (path == null ? "" : path);
  }
}
