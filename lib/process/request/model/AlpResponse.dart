class AlpResponse<T> {
  T? data;
  int? statusCode;
  String? statusMessage;
  Uri url;
  dynamic requestBody;
  Map<String, String>? headers;

  AlpResponse(
      {this.data,
      this.statusCode,
      this.statusMessage,
      required this.url,
      this.requestBody,
      this.headers});
}
