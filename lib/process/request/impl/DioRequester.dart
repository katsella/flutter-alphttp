import 'package:alp_client/process/request/IAlpRequester.dart';
import 'package:alp_client/process/request/model/AlpResponse.dart';
import 'package:dio/dio.dart';

class DioRequester implements IAlpRequester {
  final Dio _dio;

  DioRequester({Dio? dio}) : _dio = dio ?? Dio();

  @override
  Future<AlpResponse<T>> requestGet<T>(url,
      {Map<String, dynamic>? requestParam,
      Map<String, String>? headers}) async {
    Options options = getOptions(headers);
    Response<T> response =
        await _dio.get<T>(url, queryParameters: requestParam, options: options);

    AlpResponse<T> alpResponse =
        AlpResponse(url: response.realUri, requestBody: null, headers: headers);
    alpResponse.data = response.data;
    alpResponse.statusCode = response.statusCode;
    alpResponse.statusMessage = response.statusMessage;
    return alpResponse;
  }

  @override
  Future<AlpResponse<T>> requestDelete<T>(url,
      {Map<String, dynamic>? requestParam,
      Map<String, String>? headers,
      body}) async {
    Options options = getOptions(headers);
    Response<T> response = await _dio.delete<T>(url,
        queryParameters: requestParam, options: options, data: body);

    AlpResponse<T> alpResponse =
        AlpResponse(url: response.realUri, requestBody: null, headers: headers);
    alpResponse.data = response.data;
    alpResponse.statusCode = response.statusCode;
    alpResponse.statusMessage = response.statusMessage;
    return alpResponse;
  }

  @override
  Future<AlpResponse<T>> requestPatch<T>(url,
      {Map<String, dynamic>? requestParam,
      Map<String, String>? headers,
      body}) async {
    Options options = getOptions(headers);
    Response<T> response = await _dio.patch<T>(url,
        queryParameters: requestParam, options: options, data: body);

    AlpResponse<T> alpResponse =
        AlpResponse(url: response.realUri, requestBody: null, headers: headers);
    alpResponse.data = response.data;
    alpResponse.statusCode = response.statusCode;
    alpResponse.statusMessage = response.statusMessage;
    return alpResponse;
  }

  @override
  Future<AlpResponse<T>> requestPost<T>(url,
      {Map<String, dynamic>? requestParam,
      Map<String, String>? headers,
      body}) async {
    Options options = getOptions(headers);
    Response<T> response = await _dio.post<T>(url,
        queryParameters: requestParam, options: options, data: body);

    AlpResponse<T> alpResponse =
        AlpResponse(url: response.realUri, requestBody: null, headers: headers);
    alpResponse.data = response.data;
    alpResponse.statusCode = response.statusCode;
    alpResponse.statusMessage = response.statusMessage;
    return alpResponse;
  }

  @override
  Future<AlpResponse<T>> requestPut<T>(url,
      {Map<String, dynamic>? requestParam,
      Map<String, String>? headers,
      body}) async {
    Options options = getOptions(headers);
    Response<T> response = await _dio.put<T>(url,
        queryParameters: requestParam, options: options, data: body);

    AlpResponse<T> alpResponse =
        AlpResponse(url: response.realUri, requestBody: null, headers: headers);
    alpResponse.data = response.data;
    alpResponse.statusCode = response.statusCode;
    alpResponse.statusMessage = response.statusMessage;
    return alpResponse;
  }

  Options getOptions(Map<String, String>? headers) {
    Options options = Options();
    if (headers != null) {
      String? contentType = headers["content-type"];
      options.contentType = contentType;
      options.headers = headers;
    }
    return options;
  }
}
