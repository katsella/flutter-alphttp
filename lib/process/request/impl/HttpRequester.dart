import 'package:alp_client/process/request/IAlpRequester.dart';
import 'package:alp_client/process/request/model/AlpResponse.dart';
import 'package:http/http.dart';

class HttpRequester implements IAlpRequester {
  final Client _httpClient;

  HttpRequester({Client? client}) : _httpClient = client ?? Client();

  Uri _urlParamCombiner(String url, Map<String, String>? params) {
    String result = url;
    if (params == null) return Uri.parse(url);

    if (url.contains("?")) {
      result += "&";
    } else {
      if (!result.endsWith('/')) result += "/";
      result += "?";
    }
    params.forEach((key, value) {
      result += key + "=" + value.toString() + "&";
    });
    Uri uri = Uri.parse(result.substring(0, result.length - 1));
    return uri;
  }

  @override
  Future<AlpResponse<T>> requestGet<T>(url,
      {Map<String, String>? requestParam,
      String? contentType,
      Map<String, String>? headers}) async {
    Uri uri = _urlParamCombiner(url, requestParam);
    Response response = await _httpClient.get(uri, headers: headers);

    AlpResponse<T> alpResponse =
        AlpResponse(url: uri, requestBody: null, headers: headers);
    alpResponse.data = response.body as T?;
    alpResponse.statusCode = response.statusCode;
    return alpResponse;
  }

  @override
  Future<AlpResponse<T>> requestDelete<T>(url,
      {Map<String, String>? requestParam,
      String? contentType,
      Map<String, String>? headers,
      body}) async {
    Uri uri = _urlParamCombiner(url, requestParam);
    Response response = await _httpClient.delete(
      uri,
      headers: headers,
      body: body,
    );

    AlpResponse<T> alpResponse =
        AlpResponse(url: uri, requestBody: null, headers: headers);
    alpResponse.data = response.body as T?;
    alpResponse.statusCode = response.statusCode;
    return alpResponse;
  }

  @override
  Future<AlpResponse<T>> requestPatch<T>(url,
      {Map<String, String>? requestParam,
      String? contentType,
      Map<String, String>? headers,
      body}) async {
    Uri uri = _urlParamCombiner(url, requestParam);
    Response response = await _httpClient.patch(
      uri,
      headers: headers,
      body: body,
    );

    AlpResponse<T> alpResponse =
        AlpResponse(url: uri, requestBody: null, headers: headers);
    alpResponse.data = response.body as T?;
    alpResponse.statusCode = response.statusCode;
    return alpResponse;
  }

  @override
  Future<AlpResponse<T>> requestPost<T>(url,
      {Map<String, String>? requestParam,
      String? contentType,
      Map<String, String>? headers,
      body}) async {
    Uri uri = _urlParamCombiner(url, requestParam);
    Response response = await _httpClient.post(
      uri,
      headers: headers,
      body: body,
    );

    AlpResponse<T> alpResponse =
        AlpResponse(url: uri, requestBody: null, headers: headers);
    alpResponse.data = response.body as T?;
    alpResponse.statusCode = response.statusCode;
    return alpResponse;
  }

  @override
  Future<AlpResponse<T>> requestPut<T>(url,
      {Map<String, String>? requestParam,
      String? contentType,
      Map<String, String>? headers,
      body}) async {
    Uri uri = _urlParamCombiner(url, requestParam);
    Response response = await _httpClient.put(
      uri,
      headers: headers,
      body: body,
    );

    AlpResponse<T> alpResponse =
        AlpResponse(url: uri, requestBody: null, headers: headers);
    alpResponse.data = response.body as T?;
    alpResponse.statusCode = response.statusCode;
    return alpResponse;
  }
}
