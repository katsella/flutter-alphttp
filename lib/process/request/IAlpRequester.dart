import 'package:alp_client/process/request/model/AlpResponse.dart';

abstract class IAlpRequester {
  Future<AlpResponse<T>> requestGet<T>(url,
      {Map<String, String>? requestParam, Map<String, String>? headers});

  Future<AlpResponse<T>> requestPost<T>(url,
      {Map<String, String>? requestParam,
      Map<String, String>? headers,
      dynamic? body});

  Future<AlpResponse<T>> requestPatch<T>(url,
      {Map<String, String>? requestParam,
      Map<String, String>? headers,
      dynamic? body});

  Future<AlpResponse<T>> requestPut<T>(url,
      {Map<String, String>? requestParam,
      Map<String, String>? headers,
      dynamic? body});

  Future<AlpResponse<T>> requestDelete<T>(url,
      {Map<String, String>? requestParam,
      Map<String, String>? headers,
      dynamic? body});
}
