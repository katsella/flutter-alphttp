import 'package:alp_client/core/MetodType.dart';
import 'package:alp_client/exception/AlpRequestCacheException.dart';
import 'package:alp_client/exception/AlpRequestException.dart';
import 'package:alp_client/process/cache/IAlpCacheHandler.dart';
import 'package:alp_client/process/request/IAlpRequester.dart';
import 'package:alp_client/process/request/impl/DioRequester.dart';
import 'package:alp_client/process/request/model/AlpResponse.dart';
import 'package:alp_client/util/HashUtils.dart';
import 'package:dio/dio.dart';

class AlpRequest {
  final IAlpRequester _alpRequester;
  final Map<String, String> _headers;

  final Map<String, String> _requestParams = {};
  IAlpCacheHandler? cacheHandler;
  dynamic _body;
  MetodType _metodType = MetodType.GET;
  String url;
  bool _cacheEnable = false;

  AlpRequest(
      {required this.url,
      Map<String, String>? headers,
      this.cacheHandler,
      IAlpRequester? alpRequester})
      : _alpRequester = alpRequester ?? DioRequester(),
        _headers = headers ?? {};

  AlpRequest setMethod(MetodType methodType) {
    _metodType = methodType;
    return this;
  }

  AlpRequest addParam(String name, dynamic value) {
    _requestParams[name] = value.toString();
    return this;
  }

  AlpRequest setPathVariable(String name, dynamic value) {
    url = url.replaceAll("{" + name + "}", value.toString());
    return this;
  }

  AlpRequest setBody(dynamic data) {
    _body = data;
    return this;
  }

  AlpRequest setAuthentication(String token) {
    _setHeader("authentication", token);
    return this;
  }

  AlpRequest setCookie(String cookie) {
    _setHeader("cookie", cookie);
    return this;
  }

  AlpRequest addHeaders(Map<String, String> headers) {
    headers.addAll(headers);
    return this;
  }

  AlpRequest addHeader(String key, String value) {
    _headers[key] = value;
    return this;
  }

  AlpRequest setContentType(String contentType) {
    _headers["content-type"] = contentType;
    return this;
  }

  AlpRequest setCacheEnable(bool value) {
    _cacheEnable = value;
    return this;
  }

  void _setHeader(String key, String value) {
    _headers[key] = value;
  }

  Future<AlpResponse<T>> execute<T>() async {
    try {
      if (_cacheEnable && _metodType == MetodType.GET) {
        return _executeCache<T>();
      }
      return await _execute();
    } on DioError catch (_) {
      rethrow;
    } catch (e) {
      throw AlpRequestException("Error request", e, url, _body);
    }
  }

  Future<AlpResponse<T>> _executeCache<T>() async {
    if (cacheHandler == null) {
      throw AlpRequestCacheException("AlpCacheHandler not set");
    }

    int requestHash = HashUtils.hash(_requestParams);

    AlpResponse<T>? cacheData =
        cacheHandler?.getCachedData<AlpResponse<T>>(url.hashCode, requestHash);
    if (cacheData != null) {
      cacheData.statusCode = 304;
      return cacheData;
    }

    AlpResponse<T> response = await _execute();
    cacheHandler?.putCachedData(url.hashCode, requestHash, response);
    return response;
  }

  Future<AlpResponse<T>> _execute<T>() async {
    AlpResponse<T> response;
    switch (_metodType) {
      case MetodType.GET:
        response = await _alpRequester.requestGet<T>(url,
            requestParam: _requestParams, headers: _headers);
        break;
      case MetodType.POST:
        response = await _alpRequester.requestPost<T>(url,
            requestParam: _requestParams, headers: _headers, body: _body);
        break;
      case MetodType.PATCH:
        response = await _alpRequester.requestPatch<T>(url,
            requestParam: _requestParams, headers: _headers, body: _body);
        break;
      case MetodType.PUT:
        response = await _alpRequester.requestPut<T>(url,
            requestParam: _requestParams, headers: _headers, body: _body);
        break;
      case MetodType.DELETE:
        response = await _alpRequester.requestDelete<T>(url,
            requestParam: _requestParams, headers: _headers, body: _body);
        break;
    }
    if (response.data == null) {
      throw AlpRequestException("Response is null", null, url, _body);
    }
    return response;
  }
}
