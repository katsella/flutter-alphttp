import 'package:alp_client/process/AlpRequest.dart';
import 'package:alp_client/process/cache/IAlpCacheHandler.dart';
import 'package:alp_client/process/cache/impl/AlpCacheHandler.dart';
import 'package:alp_client/process/request/IAlpRequester.dart';
import 'package:alp_client/process/request/impl/DioRequester.dart';
import 'package:alp_client/util/UrlUtils.dart';

class AlpHttp {
  final Map<String, String> _headers = {};

  final IAlpCacheHandler _cacheHandler;

  final IAlpRequester _alpRequester;

  String? baseUrl;

  AlpHttp(
      {this.baseUrl,
      IAlpCacheHandler? cacheHandler,
      IAlpRequester? alpRequester})
      : _cacheHandler = cacheHandler ?? AlpCacheHandler(),
        _alpRequester = alpRequester ?? DioRequester();

  AlpHttp addHeaders(Map<String, String> headers) {
    _headers.addAll(headers);
    return this;
  }

  AlpHttp addHeader(String key, String value) {
    _headers[key] = value;
    return this;
  }

  AlpRequest request(String url) {
    return AlpRequest(
        url: UrlUtils.combineUrl(baseUrl, url),
        headers: _headers,
        cacheHandler: _cacheHandler,
        alpRequester: _alpRequester);
  }
}
