abstract class IAlpCacheHandler {
  T? getCachedData<T>(dynamic urlHash, dynamic requestDataHash);

  void putCachedData(dynamic urlHash, dynamic requestDataHash, dynamic data);
}
