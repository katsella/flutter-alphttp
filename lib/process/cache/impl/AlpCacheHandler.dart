import 'dart:collection';

import '../IAlpCacheHandler.dart';

class AlpCacheHandler implements IAlpCacheHandler {
  HashMap hashMap = HashMap<dynamic, HashMap<dynamic, dynamic>?>();

  @override
  T? getCachedData<T>(dynamic method, dynamic requestData) {
    HashMap<dynamic, dynamic>? map = hashMap[method];
    if (map == null) return null;
    T? cacheData = map[requestData];
    return cacheData;
  }

  @override
  void putCachedData(dynamic method, dynamic requestData, dynamic data) {
    HashMap<dynamic, dynamic>? map = hashMap[method];
    if (map == null) hashMap[method] = map = HashMap<dynamic, dynamic>();
    map[requestData] = data;
  }
}
