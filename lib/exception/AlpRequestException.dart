class AlpRequestException implements Exception {
  String message;
  Object? exception;
  String url;
  String? body;

  AlpRequestException(this.message, this.exception, this.url, this.body);

  @override
  String toString() {
    return 'AlpRequestException{message: $message, exception: $exception, url: $url, body: $body}';
  }
}
