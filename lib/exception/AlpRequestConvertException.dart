import 'package:alp_client/exception/AlpRequestException.dart';

class AlpRequestConvertException extends AlpRequestException {
  AlpRequestConvertException(String message, Object exception, String url, String body) : super(message, exception, url, body);

  @override
  String toString() {
    return 'AlpRequestConvertException{message: $message, exception: $exception, url: $url, body: $body}';
  }
}
